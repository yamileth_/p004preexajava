package com.example.p004preexajava;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button btnEntrar;
    private Button btnSalir;
    private EditText txtNombre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        iniciarComponentes();

        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                entrar();
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                salir();
            }
        });
    }

    private void iniciarComponentes() {
        btnEntrar = findViewById(R.id.btnEntrar);
        btnSalir = findViewById(R.id.btnSalir);
        txtNombre = findViewById(R.id.txtNombre);
    }

    private void entrar() {
        String strNombre;

        strNombre = txtNombre.getText().toString();
        if (txtNombre.getText().toString().equals("")) {
            Toast.makeText(getApplicationContext(), "Nombre no válido o no ingresado", Toast.LENGTH_LONG).show();
        } else {
            Bundle bundle = new Bundle();
            bundle.putString("Nombre", strNombre);

            Intent intent = new Intent(MainActivity.this, ActivityRecibo.class);
            intent.putExtras(bundle);
            startActivity(intent);
        }
        txtNombre.setText("");
    }

    private void salir() {
        finish();
    }
}